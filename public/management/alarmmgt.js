var unacknowledged_alarms = false; 
var measurementTypesToPrism = {
    'CHW_DP': 'DP',
    'E_TBU_DMD': 'E_TBU_DMD',
    'ANOTHER_TAG': 'ANOTHER_TAG In Prism',
    'AND_ANOTHER': 'AND_ANOTHER In Prism',
};
var timer1;
var timerAudible;
var measurementTypes = Object.keys(measurementTypesToPrism);
var ignitionMachine = "10.101.206.9:8088" // production
var ignitionMachine = "localhost:8088" // development
ignitionProject = "http://" + ignitionMachine + "/main/system/webdev/alarms/"; 
var ignitionAlarmsEndPoint = ignitionProject + "alarms"; // development
var ignitionShelveEndPoint = ignitionProject + "shelve"; // development
// var relayMachine = "10.101.206.13"; // production
var relayMachine = "localhost:83"; // development
var relayAlarmsEndPoint = "http://" + relayMachine
var relayHistoryEndPoint = "http://" + relayMachine + "/history";
var notTheseHistory = ['getsource','getlasteventstate','getactivedata','getackdata','getcleareddata'];
var prependColumnsAlarms =  ['At','Tag','Because', 'Acknowledged'];
var prependColumnsHistory = ['At','Tag','Became','Because'];
let audio = new Audio("https://www.freesound.org/data/previews/178/178032_1449999-lq.mp3")

function playAudible() { 
    audio.play()
    return audio.paused;
}
function setUpAlarmHeader(){ 
	$("#title").text("Active Alarms").css("color", "gray"); 
	$("#subtitle").text("").css("color", "gray");  
	$.get(relayAlarmsEndPoint, function(data, status){
        $('table > thead').empty(); 
		var thead = $('<thead/>'); 
		var row; 
		row = $('<tr/>'); 
		var th; 
		for(i=0; i!=prependColumnsAlarms.length; i++){
		    th = $('<th/>').text(prependColumnsAlarms[i]);
			row.append(th); 
		} 
		var trendButton = $('<input />', {type: 'button', value: 'trend'});
	    var th  = $('<th/>').append(trendButton.prop('outerHTML')); 
		// row.append(th);

        $("table thead").append(row);
    	$("table").tablesorter();  
        $("table").trigger("update"); 
        var sorting = [[1,1]]; 
        // $("table").trigger("sorton",[sorting]); 
	    $('input[type="button"][value="trend"]').click(function(){
            var prefix = 'https://energyportal.utilities.utexas.edu:4443/api/Charts/QuickTrend/Line/tags/';
	    	var checkBoxes = $('input[type="checkbox"]:checked');
	    	var tags = '';
	    	checkBoxes.each(function(index){
	    		var rowId = this.id.substring(5, this.id.length); 
		        var buildingField = $('#building'+rowId);
		        var measurementField = $('#measurement'+rowId);
		        if(index == 0)
	            	tags = buildingField.attr('value').toUpperCase() + '_' + measurementField.attr('value').toUpperCase()
	            else
	            	tags = tags + ',' + buildingField.attr('value').toUpperCase() + '_' + measurementField.attr('value').toUpperCase(); 
	    	});
            var url = prefix + tags;
            var w = window.innerWidth;
			var h = window.innerHeight;
            window.open(url, "_blank", "height=" + h + ",left=100,location=no,menubar=no,resizable=yes,scrollbars=no,status=no,titlebar=no,toolbar=no,top=100,width=" + w*2/3);
	    }); 
    });	
}
function capitalizeFirst(string) 
{
	string = (string + ""); 
    return (string + "").charAt(0).toUpperCase() + string.slice(1);
}
function pullAlarms(){ 
	console.log('pullAlarms'); 
	if(unacknowledged_alarms){
		if(timerAudible == undefined)
			timerAudible = setInterval(playAudible, 3000); 
	} else {
		clearInterval(timerAudible);
		timerAudible = undefined; 
	}
	$('input[type="button"][value="Submit"]').unbind('click'); 
	$.get(relayAlarmsEndPoint, function(data_string, status){ 
		if(data_string.indexOf("Expired") != -1)
			debugger; 
		var data = JSON.parse(data_string); 
        $('table > tbody').empty(); 
		unacknowledged_alarms = false; 
        for(i=0; i != data.rows.length; i++){
        	var theRow = data.rows[i];
	        var active = theRow[0];
	        var shelved = theRow[6];
	        if(active && !shelved){
				var sourcePath = theRow[3]; 
				var nodeId = sourcePath.split('/'); 
				nodeId = nodeId[0] + ' ' + nodeId[1]; 
	        	var acknowledged = theRow[1]; 
				if(!acknowledged)
					unacknowledged_alarms = true; 
				var uuid = theRow[5]; 
			    var posBuilding = nodeId.indexOf('/') + 1; 
			    var building = nodeId.substring(posBuilding, posBuilding + 3).toLowerCase(); 
			    var sourcePieces = theRow[3].split('/');
	        	var theSource = sourcePieces[1];
			    var constraint = theRow[4];
				constraint = constraint.substring(1,constraint.length-1); // remove {}
				constraint = constraint.split(', ');
				var constraintHash = {};
				for(elementN in constraint){
					var pieces = constraint[elementN].split('=');
					var lpiece = pieces[0];
					var rpiece = pieces[1];
					constraintHash[lpiece] = rpiece;
				}

				timeAck = constraintHash['eventTime'];
	    		because = constraintHash['mode'] + " " + constraintHash['eventValue'];

				var row = $('<tr/>'); 
	    		var td; 
			    td = $('<td/>').text(timeAck);
				row.append(td); 
			    td = $('<td/>').attr('id', 'source'+i).text(nodeId);
				row.append(td);
			    td = $('<td/>').text(because);
				row.append(td);  
			    td = $('<td/>').text(capitalizeFirst(acknowledged)); 
			    td.css('border-radius', '25px');
			    td.css('text-align', 'center');
			    td.css('background-color', 'white'); 
			    td.css('vertical-align', 'middle'); 
	    		if(acknowledged){
		    		td.css('color', 'black');
		    		td.css('background-color', '#fffe00'); 
    			} else {
		    		td.css('color', 'white');
		    		td.css('background-color', '#800000');
	    		}
				row.append(td); 

				var trendCheckBox  = $('<input />', {type: 'checkbox', id: 'trend'+i});
	        	td = $('<td/>').append(trendCheckBox.prop('outerHTML')); 
				// row.append(td); 

				var measurementType = '(not found)'; 
			    for(var j=0; j != measurementTypes.length; j++){
			        var measurementTypePossible = measurementTypes[j]; 
			        if (nodeId.indexOf(measurementTypePossible) > -1){
			            measurementType = measurementTypePossible;
			        } 
			    }
			    if(typeof(measurementTypesToPrism[measurementType]) != 'undefined')
			    	var measurementTypeToPrism = measurementTypesToPrism[measurementType].toUpperCase();
		        var td; 
				var ackButton  = $('<input />', {type: 'button', id: 'acknowledge'+i, value: 'acknowledge'});
				var shelveButton  = $('<input />', {type: 'button', id: 'shelve'+i, value: 'shelve', sourcePath: sourcePath});
				var histButton  = $('<input />', {type: 'button', id: 'history'+i, value: 'history'});
				var uuidField  = $('<input />', { type: 'hidden', id: 'uuid'+i, value: uuid, class: 'uuidClass'});
				var buildingField  = $('<input />', { type: 'hidden', id: 'building'+i, value: building});
				var measurementField  = $('<input />', { type: 'hidden', id: 'measurement'+i, value: measurementTypeToPrism});
	        	td = $('<td/>').append(
	        		ackButton.prop('outerHTML') + ' ' +
	        		shelveButton.prop('outerHTML') + ' ' +
	        		// histButton.prop('outerHTML') + ' ' +
	        		uuidField.prop('outerHTML') + ' ' +
	        		buildingField.prop('outerHTML') + ' ' +
	        		measurementField.prop('outerHTML')
	        	); 
				row.append(td);
				$('tbody').append(row);
			}
        }
        $("table").trigger("update"); 
	    $('input[type="button"][value="acknowledge"]').click(function(){
	    	var rowId = this.id.substring(11, this.id.length); 
	        var uuidField = $('#uuid'+rowId);
	        $('#submitUUID').val(uuidField.val());
	        $('#userField').val('');
	        $('#notesField').val('');
	    	$("#dialog").dialog({title: "Acknowledgement"})
			$("#dialog").dialog( "open" );
	    });
	    $('input[type="button"][value="shelve"]').click(function(){
	    	var rowId = this.id.substring("shelve".length, this.id.length); 
	        $('#rowId').val(rowId);
	    	$("#dialog_shelving").dialog({title: "Time"})
			$("#dialog_shelving").dialog( "open" );
	    });
	    $('input[type="button"][value="history"]').click(function(e){ 
	    	$("#statusButton").show();
	    	$("#historyButton").hide();
	    	var rowId = this.id.substring('history'.length, this.id.length); 
	    	var source = $("#source"+rowId).text(); 
	    	var sourcePieces = source.split(' ');
	    	var filterFor = sourcePieces[0];
	    	$("#title").text(filterFor + " Alarm History").css("color", "gray");;
	    	$("#subtitle").text('').css("color", "gray");
            setUpHistoryHeader();
            pullHistory();
            clearTimeout(timer1);
    		// timer1 = setInterval(pullHistory, 5000);
	    }); 
		$("#submitButton").click(function( event ) {
			event.preventDefault();
	        var uuid = $('#submitUUID').val();
	        var user = $('#userField').val();
			var $form = $( this ),
				notes = $('#notesField').val();
			var posting = $.post(ignitionAlarmsEndPoint, { uuid: uuid, user: user, notes: notes }, function( data ) {
				console.log( "post success" );
				var content = $( data ).find( "#content" );
				$( "#result" ).empty().append( content );
			})
			.done(function(data) {
				console.log('post done: ' + data);
			})
			.fail(function() {
				console.log( "post fail" );
			})
			.always(function() {
				console.log( "post always" );
			}); 
			$( "#dialog" ).dialog( "close" );
			return false;
		});	  
		$("#submit_shelving").click(function(event) {
			let rowId = $("#rowId")[0].value; 
	        var source_path = $($('#shelve'+rowId)[0]).attr("sourcepath");
			var shelve_time = $("#shelve_time option:selected").val();
			var posting = $.post(ignitionShelveEndPoint, {source_path: source_path, shelve_time: shelve_time}, function( data ) {
				console.log( "post success" );
				var content = $( data ).find( "#content" );
				$( "#result" ).empty().append( content );
			})
			.done(function(data) {
				console.log('post done: ' + data);
			})
			.fail(function() {
				console.log( "post fail" );
			})
			.always(function() {
				console.log( "post always" );
			}); 
			$( "#dialog_shelving" ).dialog( "close" );
			return false;
		});	    
    });	
}
function alarmStatus(){ 
	$("#statusButton").hide(); 
	$("#historyButton").show(); 
	$("#dialog").dialog({autoOpen: false, closeOnEscape: false}); 
	$("#dialog_shelving").dialog({autoOpen: false, closeOnEscape: false}); 
	setUpAlarmHeader(); 
    pullAlarms(); 
    clearTimeout(timer1); 
    timer1 = setInterval(pullAlarms, 5000); 
} 
function createHistoryHeader(data){ 
	var row; 
	row = $('<tr/>'); 
	var th;
	for(i=0; i!=prependColumnsHistory.length; i++){
	    th = $('<th/>').text(prependColumnsHistory[i]);
		row.append(th); 
	}
    $("table thead").append(row); 
}
function setUpHistoryHeader(){ 
	$.get(relayHistoryEndPoint, function(data, status){
        $('table > thead').empty(); 
        $('table > tbody').empty(); 
		createHistoryHeader(data); 
    	$("table").tablesorter();  
        $("table").trigger("update"); 
        var sorting = [[1,1]]; 
        // $("table").trigger("sorton",[sorting]);            
    });	
}
function string_contains(string0, piece){
	return string0.indexOf(piece) != -1; 
}
function normalSource(theSource){
	var rv = true; 
	rv = rv && !string_contains(theSource, "evt:"); 
	rv = rv && !string_contains(theSource, "System Startup"); 
	return rv; 
}
function pullHistory(){ 
	console.log('pullHistory');
	$.get(relayHistoryEndPoint, function(data_string, status){ 
        $('table > tbody').empty();  
		var data = JSON.parse(data_string); 
	    for(i=0; i != data.rows.length; i++){ 
	        var theRow = data.rows[i];
	        var theSource = theRow[0];
        	if(normalSource(theSource)){ 
	    		theSource = theSource.split(':'); 
	        	theSource = theSource[3].split('/')[0] + ' ' + theSource[3].split('/')[1]; 
	        	var activeData = theRow[1];
	        	var acknowledgedData = theRow[2];
	        	var clearData = theRow[3];
	        	var titleBeingUsed = $("#title").text(); 
	        	var filtering = titleBeingUsed  != "Alarm History"; 
	        	var filterFor = titleBeingUsed.split(' ')[0]; 
	        	var displayRow = theSource == filterFor; 
		    	if(!filtering || filtering && displayRow){
					var row = $('<tr/>'); 
					var timeAck = '?'; 
					var active = theRow[1].indexOf(',') != -1 ? 1 : 0;
					var acknowledged = theRow[2].indexOf(',') != -1 ? 1 : 0;
					var cleared = theRow[3].indexOf(',') != -1 ? 1 : 0;
					var state = cleared * 4 + acknowledged * 2 + active; 
					var because = '?'; 
					var source = '?'; 
		    		switch(state){ 
		    			case 1: // Active
		    				activeData = activeData.substring(1,activeData.length-1); // remove {}
		    				activeData = activeData.split(', ');
		    				var aHash = {};
		    				for(elementN in activeData){
		    					var pieces = activeData[elementN].split('=');
		    					var lpiece = pieces[0];
		    					var rpiece = pieces[1];
		    					aHash[lpiece] = rpiece;
		    				}
		    				timeAck = aHash['eventTime'];
		    				because = 'It became ' + aHash['eventValue'];
		    				break;
		    			case 2: 
		    			case 3: // Acknowledged
		    				acknowledgedData = acknowledgedData.substring(1,acknowledgedData.length-1); // remove {}
		    				acknowledgedData = acknowledgedData.split(', ');
		    				var aHash = {};
		    				for(elementN in acknowledgedData){
		    					var pieces = acknowledgedData[elementN].split('=');
		    					var lpiece = pieces[0];
		    					var rpiece = pieces[1];
		    					aHash[lpiece] = rpiece;
		    				}
		    				timeAck = aHash['eventTime'];
		    				var user = aHash['ackUser'].split(':')[1]; 
		    				if(user == 'Live Event Limit'){
		    					var reason = "it was automatically acknowledged"
		    				} else {
		    					var reason = aHash['ackNotes']
		    				}
		    				because = user + ' SAYS ' + reason;
		    				break;
		    			case 4: 
		    			case 5: 
		    			case 6: 
		    			case 7: // Cleared
		    				clearData = clearData.substring(1,clearData.length-1); // remove {}
		    				clearData = clearData.split(', ');
		    				var aHash = {};
		    				for(elementN in clearData){
		    					var pieces = clearData[elementN].split('=');
		    					var lpiece = pieces[0];
		    					var rpiece = pieces[1];
		    					aHash[lpiece] = rpiece;
		    				}
		    				timeAck = aHash['eventTime'];
		    				because = 'It became ' + aHash['eventValue'];
		    				break; 
		    			default:
		    				timeAck = '(error)';
		    		}; 
		    		switch(state){
		    			case 1:
		    				var humanState = 'Active'; 
		    				break; 
		    			case 2:
		    				var humanState = 'Acknowledged'; 
		    				break; 
		    			case 3:
		    			case 4:
		    			case 5:
		    			case 6:
		    			case 7:
		    				var humanState = 'Cleared'; 
		    				break; 
	    				default:
	    					var humanState = '(error)'; 
		    		} 
		    		var td; 
				    td = $('<td/>')
				    td.text(timeAck);
					row.append(td); 
				    td = $('<td/>').text(theSource);
					row.append(td);
				    td = $('<td/>').text(humanState); 
				    td.css('border-radius', '25px');
				    td.css('text-align', 'center');
		    		switch(state){
		    			case 1:
				    		td.css('color', 'white');
				    		td.css('background-color', '#800000'); 
		    				break; 
		    			case 2:
				    		td.css('color', 'black');
				    		td.css('background-color', '#fcff00');
		    				break; 
		    			case 3:
		    			case 4:
		    			case 5:
		    			case 6:
		    			case 7:
				    		td.css('color', 'white');
				    		td.css('background-color', '#008000'); 
		    				break; 
		    		} 
					row.append(td); 
				    td = $('<td/>').text(because);
					row.append(td); 
			    //     for(j=0; j != data.columns.length; j++){
			    //     	var columnName = data.columns[j].toLowerCase(); 
		    	// 		var posOfOneOfThese = $.inArray(data.columns[j].toLowerCase(), notTheseHistory);
		    	// 		if(posOfOneOfThese == -1){
			    //     		var td = $('<td/>').text(theRow[j]);
							// row.append(td);
			    //     	}
			    //     }
					$('tbody').append(row);
				}
		    }
	    };
	    $("table").trigger("update");
    });	
}
$(document).ready(function() { 
    $('#historyButton').click(function(e){
		$("#statusButton").show();
		$("#historyButton").hide();
    	$("#title").text('Alarm History').css("color", "gray");;
    	$("#subtitle").text('').css("color", "gray");
        setUpHistoryHeader();
        pullHistory();
        clearTimeout(timer1);
		// timer1 = setInterval(pullHistory, 5000); 
    }); 
	$('body').css("color", "gray");
	$('.tablesorter').css('color', 'black');
	$('#statusButton').click(function(){
		alarmStatus();
	});
	alarmStatus();
});