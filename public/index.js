let audio = new Audio("https://www.freesound.org/data/previews/178/178032_1449999-lq.mp3")
let initialZoom = 15; 
var timer_alarms_continuously; 
var dont_care_after = 5 * 60 * 1000; // (milliseconds)
let labelZoomLimit = 14; 
var markers = [];
var buildingLabels = [];
var showMarkers = {};
var stateChangedAt = {};
var stateChangedPreviouslyAt = {};
var buildingAlarmStates = {};

function alarm_for_public_map(alarm){
  if(alarm["escalated"]){
    var dotRedMarker = {
        path: 'm -3.8369261,9.6568904 6.9159405,-6.347507 m 0,6.347507 -6.9159402,-6.347507 M 8.2529812,6.3432583 A 8.8181108,8.8181108 0 0 1 -0.56513273,15.161365 8.8181108,8.8181108 0 0 1 -9.3832393,6.3432583 8.8181108,8.8181108 0 0 1 -0.56513273,-2.4748553 8.8181108,8.8181108 0 0 1 8.2529812,6.3432583 Z',
        fillColor: '#ffe5e6',
        fillOpacity: 1,
        scale: 0.8,
        strokeColor: 'red',
        strokeWeight: 2
    };
    var icon = dotRedMarker; 
  } else {
    var dotGreenMarker = {
        path: 'M -3.3395465,6.8620925 -0.82896543,9.1831958 3.386916,4.2567727 M 6.5923762,6.5203363 A 6.6135831,6.6135831 0 0 1 -0.02120924,13.133916 6.6135831,6.6135831 0 0 1 -6.6347892,6.5203363 6.6135831,6.6135831 0 0 1 -0.02120924,-0.09324887 6.6135831,6.6135831 0 0 1 6.5923762,6.5203363 Z',
        fillColor: '#e7ffe7',
        fillOpacity: 0.0,
        scale: 1,
        strokeColor: 'green',
        strokeWeight: 2,
        strokeOpacity: 0.25
    };
    var icon = dotGreenMarker; 
  }
  marker = new google.maps.Marker({ 
      position: alarm["where"], 
      map: map, 
      title: alarm["who"].split('/')[0],
      icon: icon, 
  }); 
  markers.push(marker); 

  var building_location = new google.maps.LatLng(alarm["where"]); 
  var zoom_level = map.getZoom();
  if(zoom_level >= labelZoomLimit){
    var label_style = "building-name-style-close"; 
  } else {
    var label_style = "building-name-style-far"; 
  }
  if(alarm["alarming"]){
    var buildingLabel = new TxtOverlay(building_location, alarm["who"].split("/")[0], label_style, map);
    buildingLabels.push(buildingLabel); 
  }; 
}

function alarm_for_public_table(tbody, alarm){
  let alarming = alarm["alarming"];
  let current_time = Date.now(); 
  let event_time = alarm["when_alarmed"];

  var row = $('<tr></tr>'); 

  var building = $('<td></td>').text(alarm["who"].split("/")[0]); 
  row.append(building); 

  var detected_at = $('<td></td>').text((new Date(event_time)).toString()); 
  row.append(detected_at); 

  var became = $('<td></td>').addClass('alarms_for_public_became_td').text(alarming); 
  if(alarming){
    became.addClass('pill_red');
  } else {
    became.addClass('pill_green');
  }
  row.append(became);

  if(alarm["age"] < 60 * 1000){
    var age = alarm["age"] / 1000; 
    var age_unit = ' secs ago';
  } else if(alarm["age"] < 60 * 60 * 1000) {
    var age = alarm["age"] / 60 / 1000; 
    var age_unit = ' mins ago';
  } else {
    var age = alarm["age"] / 60 / 60 / 1000; 
    var age_unit = ' hrs ago';
  }
  var age = parseInt(Math.round(age).toFixed(0));

  var pElement = $('<p>'); 
  pElement.addClass("unit"); 
  pElement.text(age_unit); 
  var since = $('<td></td>').text(age); 
  since.append(pElement);         
  row.append(since); 

  tbody.append(row);
}

function alarms_for_public_map(){
  $.get(alarm_service + '/alarms_for_public_map', function(body, status){
    var alarms = JSON.parse(body); 
    markers.forEach(function(marker){
      marker.setMap(null); 
    });
    buildingLabels.forEach(function(buildingLabel){
      buildingLabel.setMap(null); 
    });
    for(alarm of alarms){ 
      alarm_for_public_map(alarm);
    } 
  }); 
} 

function alarms_for_public_table(){
  $.get(alarm_service + '/alarms_for_public_table', function(body, status){
    var alarm_data = JSON.parse(body); 
    var thead = $('#alarm_summary > thead').empty();
    var tbody = $('#alarm_summary > tbody').empty(); 
    if(alarm_data.tags.length == 0){
      $("#outage-count").text("No Power Outages");
      tbody.append($("<tr></tr>").text("No Power Outages")); 
    } else {
	    if(alarm_data.tags.length == 1){
			$("#outage-count").text(alarm_data.alarming_count + " power outage");
		} else {
			$("#outage-count").text(alarm_data.alarming_count + " power outages");
		}
      thead.append($("<tr><th>Building</th><th>Detected</th><th>Became</th><th>Since</th></tr>"))
      for(alarm of alarm_data.tags){
        if(alarm.alarming || (!alarm.alarm && alarm.recently_changed)){
          alarm_for_public_table(tbody, alarm);
        }
      } 
    }
  }); 
}  

function alarms_continuously(){  
  alarms_for_public_table();
  alarms_for_public_map(); 
} 

$(document).ready(function() { 
  $( "#accordion" ).accordion({
    heightStyle: "content"
  });

  $('body').css("color", "gray");

  var latlngc = new google.maps.LatLng(30.2845, -97.735);
  var mapOptions = {
      gestureHandling: 'greedy', 
      zoom: initialZoom,
      center: latlngc,
      mapTypeId: google.maps.MapTypeId.ROADMAP,
      styles: [
        {
          "elementType": "geometry",
          "stylers": [
            {
              "color": "#f5f5f5"
            }
          ]
        },
        {
          "elementType": "labels.icon",
          "stylers": [
            {
              "visibility": "off"
            }
          ]
        },
        {
          "elementType": "labels.text",
          "stylers": [
            {
              "visibility": "off"
            }
          ]
        },
        {
          "elementType": "labels.text.fill",
          "stylers": [
            {
              "color": "#616161"
            }
          ]
        },
        {
          "elementType": "labels.text.stroke",
          "stylers": [
            {
              "color": "#f5f5f5"
            }
          ]
        },
        {
          "featureType": "administrative.land_parcel",
          "elementType": "labels.text.fill",
          "stylers": [
            {
              "color": "#bdbdbd"
            }
          ]
        },
        {
          "featureType": "poi",
          "elementType": "geometry",
          "stylers": [
            {
              "color": "#eeeeee"
            }
          ]
        },
        {
          "featureType": "poi",
          "elementType": "labels.text.fill",
          "stylers": [
            {
              "color": "#757575"
            }
          ]
        },
        {
          "featureType": "poi.park",
          "elementType": "geometry",
          "stylers": [
            {
              "color": "#e5e5e5"
            }
          ]
        },
        {
          "featureType": "poi.park",
          "elementType": "labels.text.fill",
          "stylers": [
            {
              "color": "#9e9e9e"
            }
          ]
        },
        {
          "featureType": "road",
          "elementType": "geometry",
          "stylers": [
            {
              "color": "#ffffff"
            }
          ]
        },
        {
          "featureType": "road.arterial",
          "elementType": "labels.text.fill",
          "stylers": [
            {
              "color": "#757575"
            }
          ]
        },
        {
          "featureType": "road.highway",
          "elementType": "geometry",
          "stylers": [
            {
              "color": "#dadada"
            }
          ]
        },
        {
          "featureType": "road.highway",
          "elementType": "labels.text.fill",
          "stylers": [
            {
              "color": "#616161"
            }
          ]
        },
        {
          "featureType": "road.local",
          "elementType": "labels.text.fill",
          "stylers": [
            {
              "color": "#9e9e9e"
            }
          ]
        },
        {
          "featureType": "transit.line",
          "elementType": "geometry",
          "stylers": [
            {
              "color": "#e5e5e5"
            }
          ]
        },
        {
          "featureType": "transit.station",
          "elementType": "geometry",
          "stylers": [
            {
              "color": "#eeeeee"
            }
          ]
        },
        {
          "featureType": "water",
          "elementType": "geometry",
          "stylers": [
            {
              "color": "#c9c9c9"
            }
          ]
        },
        {
          "featureType": "water",
          "elementType": "labels.text.fill",
          "stylers": [
            {
              "color": "#9e9e9e"
            }
          ]
        }
      ]        
  }; 
  map = new google.maps.Map(document.getElementById("map"), mapOptions);
  alarms_continuously();
  clearTimeout(timer_alarms_continuously); 
  timer_alarms_continuously = setInterval(alarms_continuously, 10000); 
}); 