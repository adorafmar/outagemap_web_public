var config = {
  development: {
    port: 1026,
    ports: 1027
  },
  default: {
    port: 80,
    ports: 443
  }
}

exports.get = function get(env) {
  return config[env] || config.default;
}