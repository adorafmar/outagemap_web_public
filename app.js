const debug  = require('debug')('tags')
console.log("DEBUG: " + process.env.DEBUG);
const port = require('./config.js').get(process.env.NODE_ENV).port;
const ports = require('./config.js').get(process.env.NODE_ENV).ports;
console.log("NODE_ENV: " + process.env.NODE_ENV);

require('dotenv').config()
var fs = require('fs')
const http = require('http');
var https = require('https') 
var express = require('express');
var path = require('path');

var app = express();

app.set('port', port);
app.enable('trust proxy');
app.use(function(req, res, next) {
  if(!req.secure) {
    var secureUrl = "https://" + req.headers['host'] + req.url; 
    res.writeHead(301, { "Location":  secureUrl });
    res.end();
  }
  next();
});

app.use(express.static(path.join(__dirname, 'config')));
app.use(express.static(path.join(__dirname, 'public')));

const httpServer = http.createServer(app);
const httpsServer = https.createServer({
  key: fs.readFileSync('outagemap_utilities_utexas_edu.key'),
  cert: fs.readFileSync('outagemap_utilities_utexas_edu_cert.cer.only.cer')
}, app);

httpServer.listen(80, () => {
    console.log('HTTP Server running on port 80');
});

httpsServer.listen(443, () => {
    console.log('HTTPS Server running on port 443');
});